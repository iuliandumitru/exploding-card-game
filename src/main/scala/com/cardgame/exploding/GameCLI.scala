package com.cardgame.exploding

import com.cardgame.exploding.Command.{DrawCard, QuitGame, Unknown}
import com.cardgame.exploding.GameResult.{GameExited, PlayerLost}

import scala.io.StdIn

/**
 * @author Iulian Dumitru
 */
object GameCLI {

  def main(args: Array[String]): Unit = {

    val nrBlankCards = 16
    val nrExplosiveCards = 1
    val nrDefuseCards = 3

    println(
      s"""
         |---------------------
         | Exploding card game
         |---------------------
         | Nr of blank cards: $nrBlankCards
         | Nr of explosive cards: $nrExplosiveCards
         | Nr of defuse cards: $nrDefuseCards (1 for the player)
         |---------------------
         |""".stripMargin
    )

    val game = Game(
      cardDeck = CardDeck.of(nrBlankCards, nrExplosiveCards, nrDefuseCards - 1).shuffle,
      commandProvider = CLICommandProvider,
      thePlayer = Player(nrDefuseCards = 1)
    )

    val result = game.play()
    result match {
      case PlayerLost => println(
        """
          |-------------------------------------------------------
          |You draw an explosive card and don't have a defuse card.
          |You lost!
          |-------------------------------------------------------
          |""".stripMargin)
      case GameExited => println(
        """
          |------------------------
          |You quited the game.
          |Bye!
          |------------------------
          |""".stripMargin)
      case GameResult.GameError => println("Unexpected problem. Please contact the admin!")
    }

  }

  object CLICommandProvider extends CommandProvider {

    override def nextCommand: Command = {
      println(s"Draw a card using 'd' or quit the game using 'q':")
      val stringCommand = StdIn.readLine()
      println(s"Your command: $stringCommand")
      stringCommand match {
        case "d" => DrawCard
        case "q" => QuitGame
        case _ => Unknown
      }
    }

  }

}
