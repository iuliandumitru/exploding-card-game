package com.cardgame.exploding

import com.cardgame.exploding.Command.{DrawCard, QuitGame, Unknown}
import com.cardgame.exploding.DrawResult.{ContinueDraw, StopDraw}
import com.cardgame.exploding.GameResult.{GameExited, PlayerLost}

import scala.annotation.tailrec

/**
 * @author Iulian Dumitru
 */

case class Game(cardDeck: CardDeck, commandProvider: CommandProvider, thePlayer: Player = Player()) {

  def play(): GameResult = newRound(cardDeck, thePlayer)

  @tailrec
  private def newRound(deck: CardDeck, player: Player): GameResult = {
    println(deck)
    commandProvider.nextCommand match {
      case DrawCard => drawNewCard(deck, player)
      case QuitGame => GameExited
      case Unknown => newRound(deck, player)
    }
  }

  private def drawNewCard(deck: CardDeck, player: Player): GameResult = {
    player.draw(deck) match {
      case (updatedPlayer, updatedDeck, drawResult) => drawResult match {
        case ContinueDraw => newRound(updatedDeck, updatedPlayer)
        case StopDraw => PlayerLost
      }
    }
  }
}


sealed trait GameResult

object GameResult {

  case object PlayerLost extends GameResult

  case object GameExited extends GameResult

  case object GameError extends GameResult

}
