package com.cardgame.exploding

import com.cardgame.exploding.DrawResult.{ContinueDraw, StopDraw}

/**
 * @author Iulian Dumitru
 */
case class Player(hand: List[Card] = Nil) {

  final def draw(cardDeck: CardDeck): (Player, CardDeck, DrawResult) = {
    cardDeck.draw match {
      case (Some(blankCard), newDeck) if blankCard.isBlank => (this, newDeck, ContinueDraw)
      case (Some(defuseCard), newDeck) if defuseCard.isDefuse => (Player(hand :+ defuseCard), newDeck, ContinueDraw)
      case (Some(explosiveCard), newDeck) if explosiveCard.isExplosive =>
        if (hasDefuseCard) {
          val shuffledDeck = CardDeck(cards = newDeck.cards :+ explosiveCard).shuffle
          (Player(handWithoutDefuseCard), shuffledDeck, ContinueDraw)
        } else {
          (this, newDeck, StopDraw)
        }
      case _ => (this, cardDeck, StopDraw)
    }
  }

  private def hasDefuseCard: Boolean = hand.count(_.isDefuse) >= 1

  private def handWithoutDefuseCard: List[Card] = {
    hand.find(_.isDefuse) match {
      case Some(defuseCard) => hand.diff(List(defuseCard))
      case None => hand
    }
  }

}

case object Player {
  def apply(nrDefuseCards: Int): Player = Player(
    hand = List.fill(nrDefuseCards)(DefuseCard())
  )
}

trait DrawResult

object DrawResult {

  case object ContinueDraw extends DrawResult

  case object StopDraw extends DrawResult

}