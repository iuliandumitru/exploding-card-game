package com.cardgame.exploding

/**
 * @author Iulian Dumitru
 */
trait CommandProvider {
  def nextCommand: Command
}

sealed trait Command

object Command {

  case object DrawCard extends Command {
    override def toString: String = "d"
  }

  case object QuitGame extends Command {
    override def toString: String = "q"
  }

  case object Unknown extends Command {
    override def toString: String = "unknown command"
  }

}

