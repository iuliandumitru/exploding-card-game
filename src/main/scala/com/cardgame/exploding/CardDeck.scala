package com.cardgame.exploding

import java.util.UUID

import scala.util.Random

/**
 * @author Iulian Dumitru
 */

sealed trait Card {

  def id: UUID

  def isBlank: Boolean = false

  def isExplosive: Boolean = false

  def isDefuse: Boolean = false

}

final case class ExplosiveCard(id: UUID = UUID.randomUUID()) extends Card {

  override def isExplosive: Boolean = true

  override def toString: String = "<E>"

}

final case class BlankCard(id: UUID = UUID.randomUUID()) extends Card {

  override def isBlank: Boolean = true

  override def toString: String = "<B>"

}

final case class DefuseCard(id: UUID = UUID.randomUUID()) extends Card {

  override def isDefuse: Boolean = true

  override def toString: String = "<D>"

}


case class CardDeck(cards: List[Card]) {

  def nrCards: Int = cards.size

  def shuffle: CardDeck = CardDeck(cards = Random.shuffle(cards))

  def draw: (Option[Card], CardDeck) =
    if (cards.isEmpty) {
      (None, CardDeck(Nil))
    } else {
      val remainingCards = cards.drop(1)
      (Some(cards.head), CardDeck(remainingCards))
    }

  override def toString: String = s"[${cards.mkString(",")}]"

}

object CardDeck {

  def of(nrBlankCards: Int, nrExplosiveCards: Int, nrDefuseCards: Int): CardDeck = {
    val blankCards = List.fill(nrBlankCards)(BlankCard())
    val explosiveCards = List.fill(nrExplosiveCards)(ExplosiveCard())
    val defuseCards = List.fill(nrDefuseCards)(DefuseCard())
    CardDeck(cards = blankCards ++ explosiveCards ++ defuseCards)
  }

}