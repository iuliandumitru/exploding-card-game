package com.cardgame.exploding

import com.cardgame.exploding.DrawResult.{ContinueDraw, StopDraw}

/**
 * @author Iulian Dumitru
 */
class PlayerSpec extends UnitSpec {

  "A player" must "draw cards from a deck of cards and ignore blank card" in {

    val blankCard1 = BlankCard()
    val blankCard2 = BlankCard()

    val deck = CardDeck(
      cards = List(blankCard1, blankCard2)
    )

    val player = Player(
      hand = List()
    )

    val (updatedPlayer, updatedDeck, result) = player.draw(deck)
    updatedPlayer.hand mustBe Nil
    updatedDeck.cards mustBe List(blankCard2)
    result mustBe ContinueDraw

  }

  it must "keep in hand a defuse card if he has drawn one" in {

    val defuseCard = DefuseCard()
    val blankCard = BlankCard()

    val deck = CardDeck(
      cards = List(defuseCard, blankCard)
    )

    val player = Player(
      hand = List()
    )

    val (updatedPlayer, updatedDeck, result) = player.draw(deck)
    updatedPlayer.hand mustBe List(defuseCard)
    updatedDeck.cards mustBe List(blankCard)
    result mustBe ContinueDraw

  }

  it must "discard his own defuse card and return the explosive card to the draw pile and re-shuffle the pile" in {
    val deck = CardDeck(
      cards = List(ExplosiveCard(), DefuseCard(), BlankCard())
    )

    val player = Player(
      hand = List(DefuseCard())
    )

    val (updatedPlayer, updatedDeck, result) = player.draw(deck)
    updatedPlayer.hand mustBe Nil
    updatedDeck.cards.count(_.isExplosive) mustBe 1
    updatedDeck.cards.count(_.isDefuse) mustBe 1
    updatedDeck.cards.count(_.isBlank) mustBe 1
    result mustBe ContinueDraw
  }

  it must "stop drawing cards if he draw an explosive card and does not have a defuse card" in {

    val deck = CardDeck(
      cards = List(ExplosiveCard(), DefuseCard(), BlankCard())
    )

    val player = Player(
      hand = List()
    )

    val (updatedPlayer, updatedDeck, result) = player.draw(deck)
    updatedPlayer.hand mustBe Nil
    result mustBe StopDraw

  }

}
