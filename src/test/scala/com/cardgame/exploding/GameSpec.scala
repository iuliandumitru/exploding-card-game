package com.cardgame.exploding

import com.cardgame.exploding.Command.{DrawCard, QuitGame, Unknown}
import com.cardgame.exploding.GameResult.PlayerLost

/**
 * @author Iulian Dumitru
 */
class GameSpec extends UnitSpec {

  case class StaticCommandProvider(commands: List[Command]) extends CommandProvider {

    private val iterator = commands.iterator

    override def nextCommand: Command = {
      iterator.next()
    }

  }

  "A game" must "be played using draw commands" in {

    val cardDeck = CardDeck(
      cards = List(BlankCard(), BlankCard(), ExplosiveCard(), BlankCard())
    )

    val provider = StaticCommandProvider(
      commands = List(DrawCard, DrawCard, DrawCard)
    )

    val game = Game(cardDeck, provider)

    val result = game.play()
    result mustBe PlayerLost

  }

  it must "support known commands and ignore unknown commands" in {

    val cardDeck = CardDeck(
      cards = List(BlankCard(), BlankCard(), ExplosiveCard(), BlankCard())
    )

    val provider = StaticCommandProvider(
      commands = List(Unknown, DrawCard, Unknown, DrawCard, DrawCard)
    )

    val game = Game(cardDeck, provider)

    val result = game.play()
    result mustBe PlayerLost
  }

  it must "support quit command" in {

    val cardDeck = CardDeck(
      cards = List(BlankCard(), BlankCard(), ExplosiveCard(), BlankCard())
    )

    val provider = StaticCommandProvider(
      commands = List(Unknown, DrawCard, QuitGame)
    )

    val game = Game(cardDeck, provider)

    val result = game.play()
    result mustBe GameResult.GameExited

  }

  it must "start with a shuffled card deck and must eventually finish with player lost result" in {

    val cardDeck = CardDeck.of(nrBlankCards = 16, nrExplosiveCards = 1, nrDefuseCards = 2).shuffle

    // will provide enough DrawCard commands so in the end player loses
    val provider = StaticCommandProvider(
      commands = List.fill(cardDeck.nrCards * 2)(DrawCard)
    )

    val game = Game(cardDeck, provider, thePlayer = Player(hand = List(DefuseCard())))

    val result = game.play()
    result mustBe PlayerLost

  }

  it must "be eventually lost by a player with 1 defuse card, 3 blank cards and 1 explosive" in {

    val cardDeck = CardDeck(
      cards = List(
        BlankCard(), BlankCard(), BlankCard(), ExplosiveCard(), DefuseCard()
      )
    )

    val provider = StaticCommandProvider(
      commands = List(DrawCard, DrawCard, DrawCard, DrawCard, DrawCard, DrawCard, DrawCard)
    )

    val game = Game(cardDeck, provider, thePlayer = Player(nrDefuseCards = 1))

    val result = game.play()
    result mustBe PlayerLost

  }


}
