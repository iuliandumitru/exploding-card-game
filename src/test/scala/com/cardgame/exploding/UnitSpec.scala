package com.cardgame.exploding

import org.scalatest.{FlatSpec, MustMatchers}

/**
 * @author Iulian Dumitru
 */
trait UnitSpec extends FlatSpec with MustMatchers
