package com.cardgame.exploding

/**
 * @author Iulian Dumitru
 */
class CardDeckSpec extends UnitSpec {

  "A card deck" must "be created from cards and maintain the total nr of cards" in {

    val deck = CardDeck(
      cards = List(BlankCard(), ExplosiveCard())
    )

    deck.nrCards mustBe 2

  }

  it must "be able to shuffle the cards" in {

    val blankCards = List(new BlankCard, new BlankCard, new BlankCard)
    val explosiveCards = List(new ExplosiveCard, new ExplosiveCard)

    val deck = CardDeck(
      cards = blankCards ++ explosiveCards
    )

    val newCardDeck = deck.shuffle

    newCardDeck.nrCards mustBe deck.nrCards
    newCardDeck.cards.count(_.isBlank) mustBe blankCards.size
    newCardDeck.cards.count(_.isExplosive) mustBe explosiveCards.size

  }

  it must "leave 1 card after drawing 1 card out of 2" in {

    val deck = CardDeck(
      cards = List(new BlankCard, new ExplosiveCard)
    )

    deck.draw match {
      case (Some(card), newDeck) =>
        card.isBlank mustBe true
        newDeck.nrCards mustBe 1

      case _ => fail("Invalid draw operation")
    }

  }

  it must "leave no cards after drawing 1 card out of 1" in {

    val deck = CardDeck(
      cards = List(new ExplosiveCard)
    )

    deck.draw match {
      case (Some(card), newDeck) =>
        card.isExplosive mustBe true
        newDeck.nrCards mustBe 0

      case _ => fail("Invalid draw operation")
    }

  }

  it must "be created from configurable nr of blank, explosive and defuse cards" in {

    val cardDeck = CardDeck.of(nrBlankCards = 16, nrExplosiveCards = 1, nrDefuseCards = 3)
    cardDeck.cards.count(_.isBlank) mustBe 16
    cardDeck.cards.count(_.isExplosive) mustBe 1
    cardDeck.cards.count(_.isDefuse) mustBe 3

  }

}
