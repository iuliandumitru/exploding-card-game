ThisBuild / scalaVersion := "2.13.1"
ThisBuild / version := "1.0"

mainClass in(Compile, run) := Some("com.cardgame.exploding.GameCLI")

lazy val exploding = (project in file("."))
  .settings(
    name := "exploding-card-game",
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % Test
  )
